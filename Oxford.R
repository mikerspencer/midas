precip = dbGetQuery(MIDAS.db, "SELECT Date, Precip FROM precip WHERE Station=606 AND strftime('%m', Date)='10'")
precip$Date = as.Date(precip$Date)
temp = dbGetQuery(MIDAS.db, "SELECT Date, MaxAirTemp, MinAirTemp FROM temperature WHERE Station=606 AND strftime('%m', Date)='10'")
temp$Date = as.Date(temp$Date)
temp = data.frame(Date = temp$Date, meantemp = (temp$MaxAirTemp+temp$MinAirTemp)/2)

Oxford = merge(precip, temp, by="Date")

png("~/Copy/Michael/Uni_temp/Oxford.png", height=600)
par(mfrow=c(2,1))
boxplot(meantemp~strftime(Date, "%d"), data=temp, xlab="Day of October", ylab="Mean temperature (deg C)", main="Temperature")
boxplot(log(Precip)~strftime(Date, "%d"), data=precip, xlab="Day of October", ylab="Log depth of precipitation (mm)", main="Precipitation")
dev.off()
